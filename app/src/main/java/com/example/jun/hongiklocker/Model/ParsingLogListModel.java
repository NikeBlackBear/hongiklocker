package com.example.jun.hongiklocker.Model;

import java.util.ArrayList;

/**
 * Created by jun on 2015-09-15.
 */
public class ParsingLogListModel {
    private String success;
    private String message;
    private ArrayList<LogListModel> messageinfo;

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<LogListModel> getMessageinfo() {
        return messageinfo;
    }

    public void setMessageinfo(ArrayList<LogListModel> messageinfo) {
        this.messageinfo = messageinfo;
    }

    public String getMessage() {
        return message;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSuccess() {
        return success;
    }
}
