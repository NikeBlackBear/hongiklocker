package com.example.jun.hongiklocker.ServerProxy;

import com.example.jun.hongiklocker.Utils.LogUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by jun on 2015-09-08.
 */
public class RequestParms {
    private HashMap<String, String> mParamsMap = new HashMap<String, String>();
    private String mTargetURL;

    public void put(String key, int value) {
        LogUtil.LOGD("RequestParams :: put() key = " + key + " value = " + value);
        mParamsMap.put(key, String.valueOf(value));
    }

    public void put(String key, String value) {
        LogUtil.LOGD("RequestParams :: put() key = " + key + " value = " + value);
        mParamsMap.put(key, value);
    }

    public HashMap<String, String> getParamsMap() {
        return mParamsMap;
    }

    public void setTargetURL(String targetURL) {
        mTargetURL = targetURL;
    }

    public String buildRequestURL() {
        StringBuffer buffer = new StringBuffer(mTargetURL);
        if (mParamsMap.size() > 0) {
            LogUtil.LOGE("RequestParams :: buildRequestURL() :: params size = " + mParamsMap.size());
            Iterator<Map.Entry<String, String>> iterator = mParamsMap.entrySet().iterator();
            buffer.append("?");
            if (iterator.hasNext()) {
                Map.Entry<String, String> firstEntry = iterator.next();
                buffer.append(firstEntry.getKey()).append("=").append(transformutf(firstEntry.getValue()));
                while (iterator.hasNext()) {
                    Map.Entry<String, String> nextEntry = iterator.next();
                    buffer.append("&").append(nextEntry.getKey()).append("=").append(transformutf(nextEntry.getValue()));
                }
            }
            LogUtil.LOGE("RequestParams :: buildRequestURL() :: result URL = " + buffer.toString());
        } else {
            LogUtil.LOGE("RequestParams :: buildRequestURL() :: NOT EXISTS REQUEST PARAMETER");
        }
        return buffer.toString();
    }


    //UTF-8
    public String transformutf(String hangle) {
        String utf = null;
        try {
            utf = URLEncoder.encode(hangle, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            LogUtil.LOGV("RequestParams :: transformutf :: encording error");
        }
        return utf;
    }
}