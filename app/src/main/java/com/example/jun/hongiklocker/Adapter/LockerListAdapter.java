package com.example.jun.hongiklocker.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jun.hongiklocker.Model.LockerInfoModel;
import com.example.jun.hongiklocker.R;

import java.util.ArrayList;

/**
 * Created by jun on 2015-07-20.
 */
public class LockerListAdapter extends BaseAdapter {
    private Activity mActivity;
    private ArrayList<LockerInfoModel> mlist;
    private LayoutInflater mInflater;
    private OnOpenBtnClickListener mOnOpenBtnClickListener;
    private OnInstentBtnClickListener mOnInstentBtnClickListener;
    private OnCloseBtnClickListener mOnCloseBtnClickListener;

    public LockerListAdapter(Activity c, ArrayList<LockerInfoModel> list){
        mActivity =c;
        mInflater = LayoutInflater.from(c);
        mlist = list;
    }

    public void setOnOpenBtnClickListener(OnOpenBtnClickListener listener) {
        mOnOpenBtnClickListener = listener;
    }
    public void setOnCloseBtnClickListener(OnCloseBtnClickListener listener) {
        mOnCloseBtnClickListener = listener;
    }

    public void setOnInstentBtnClickListener(OnInstentBtnClickListener listener) {
        mOnInstentBtnClickListener = listener;
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public LockerInfoModel getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.locker_list_row, null);
            holder = new ViewHolder();
            holder.closeBtn = (TextView)convertView.findViewById(R.id.close_btn);
            holder.major = (TextView)convertView.findViewById(R.id.major);
            holder.number = (TextView)convertView.findViewById(R.id.number);
            holder.openBtn = (TextView)convertView.findViewById(R.id.open_btn);
            holder.instentBtn = (TextView)convertView.findViewById(R.id.instent);
            holder.openBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer)v.getTag();
                    mOnOpenBtnClickListener.onClick(getItem(position));
                }
            });
            holder.closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer)v.getTag();
                    mOnCloseBtnClickListener.onClick(getItem(position));
                }
            });
            holder.instentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer)v.getTag();
                    mOnInstentBtnClickListener.onClick(getItem(position));
                }
            });
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        convertView.setTag(holder);
        holder.openBtn.setTag(position);
        holder.closeBtn.setTag(position);
        holder.instentBtn.setTag(position);
        holder.major.setText(getItem(position).getLu_code_name());
        holder.number.setText(getItem(position).getLu_num());

        return convertView;
    }

    public interface OnOpenBtnClickListener{
        public void onClick(LockerInfoModel data);
    }
    public interface OnCloseBtnClickListener{
        public void onClick(LockerInfoModel data);
    }
    public interface OnInstentBtnClickListener{
        public void onClick(LockerInfoModel data);
    }
    private class ViewHolder{
        TextView major;
        TextView number;
        TextView openBtn;
        TextView instentBtn;
        TextView closeBtn;
    }
}
