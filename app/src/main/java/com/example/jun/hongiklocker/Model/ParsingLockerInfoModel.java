package com.example.jun.hongiklocker.Model;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-09-11.
 */
public class ParsingLockerInfoModel {
    private String success;
    private String message;
    private String total;
    private ArrayList<LockerInfoModel> messageinfo;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public ArrayList<LockerInfoModel> getMessageinfo() {
        return messageinfo;
    }

    public void setMessageinfo(ArrayList<LockerInfoModel> messageinfo) {
        this.messageinfo = messageinfo;
    }

    public String getMessage() {
        return message;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }
    @Override
    public String toString(){
        return "success = " + success + "// message = "+ message + "// total = " + total;
    }
}
