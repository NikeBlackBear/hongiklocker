package com.example.jun.hongiklocker.Model;

/**
 * Created by jun on 2015-09-09.
 */
public class DefaultServerParsingModel {
    private String success;
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSuccess() {
        return success;
    }
    @Override
    public String toString(){
        return "success = " + success + "// message = "+ message;
    }
}
