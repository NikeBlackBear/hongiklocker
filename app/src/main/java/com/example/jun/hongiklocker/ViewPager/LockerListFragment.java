package com.example.jun.hongiklocker.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


import com.example.jun.hongiklocker.Adapter.LockerListAdapter;
import com.example.jun.hongiklocker.Model.DefaultServerParsingModel;
import com.example.jun.hongiklocker.Model.LockerInfoModel;
import com.example.jun.hongiklocker.Model.ParsingLockerInfoModel;
import com.example.jun.hongiklocker.R;
import com.example.jun.hongiklocker.ServerProxy.ParseManager;
import com.example.jun.hongiklocker.ServerProxy.ServerProxy;
import com.example.jun.hongiklocker.Utils.CommonValues;
import com.example.jun.hongiklocker.Utils.LogUtil;

/**
 * Created by jun on 2015-07-19.
 */
public class LockerListFragment extends Fragment implements CommonValues{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String user_id;
    private LockerListAdapter mLockerListAdapter;
    private ListView lockerListView;
    private ImageView emptylocker;

    private String mParam1;
    private String mParam2;


    public static LockerListFragment newInstance(String param1, String param2) {
        LockerListFragment fragment = new LockerListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public LockerListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        ServerProxy.getInstance(getActivity()).getLockerInfo(getLockerInfoCallback, user_id);
    }

    private void init() {
        View view = getView();
        Intent intent = getActivity().getIntent();
        user_id = intent.getStringExtra("id");
        SharedPreferences pref = getActivity().getSharedPreferences("pref", getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(USER_ID, user_id);
        editor.commit();
        lockerListView = (ListView) view.findViewById(R.id.lockerList);
        emptylocker = (ImageView) view.findViewById(R.id.emptylocker);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lock_list_fragment, container, false);
    }



    private LockerListAdapter.OnInstentBtnClickListener onInstentBtnClickListener = new LockerListAdapter.OnInstentBtnClickListener() {
        @Override
        public void onClick(LockerInfoModel data) {
            //인스턴트 코드 생성
            Toast.makeText(getActivity(),"구현중",Toast.LENGTH_SHORT).show();
        }
    };
    private LockerListAdapter.OnCloseBtnClickListener onOFFClickListener = new LockerListAdapter.OnCloseBtnClickListener() {
        @Override
        public void onClick(LockerInfoModel data) {
            ServerProxy.getInstance(getActivity()).submitOnOffSignal(onCallback, user_id, data.getLu_code(), data.getLu_num(), "off");
        }
    };

    private LockerListAdapter.OnOpenBtnClickListener onONClickListener = new LockerListAdapter.OnOpenBtnClickListener() {
        @Override
        public void onClick(LockerInfoModel data) {
            ServerProxy.getInstance(getActivity()).submitOnOffSignal(offCallback,user_id,data.getLu_code(),data.getLu_num(),"on");
        }
    };
    private ServerProxy.ResponseCallback getLockerInfoCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("LockerListFragment :: getLockerInfoCallback :: onResponse() :: response = " + response);
                ParsingLockerInfoModel Modeltemp = new ParseManager().LockerInfoparsing(response);
                if (Modeltemp.getSuccess().equals("1")) {
                    mLockerListAdapter = new LockerListAdapter(getActivity(), Modeltemp.getMessageinfo());
                    lockerListView.setAdapter(mLockerListAdapter);
                    mLockerListAdapter.setOnCloseBtnClickListener(onOFFClickListener);
                    mLockerListAdapter.setOnInstentBtnClickListener(onInstentBtnClickListener);
                    mLockerListAdapter.setOnOpenBtnClickListener(onONClickListener);
                    emptylocker.setVisibility(View.GONE);
                    lockerListView.setVisibility(View.VISIBLE);
                } else {
                    emptylocker.setVisibility(View.VISIBLE);
                    lockerListView.setVisibility(View.GONE);
                }
            } else {
                LogUtil.LOGV("LockerListFragment :: getLockerInfoCallback :: not exists next data");
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("LockerListFragment :: getLockerInfoCallback :: onError() :: status =" + status);
        }
    };
    private ServerProxy.ResponseCallback onCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("LockerListFragment :: onCallback :: onResponse() :: response = " + response);
                DefaultServerParsingModel Modeltemp = new ParseManager().DefaultServerParsing(response);
                if (Modeltemp.getSuccess().equals("1")){
                    Toast.makeText(getActivity(),"사물함 열림",Toast.LENGTH_SHORT).show();
                }
            } else {
                LogUtil.LOGV("LockerListFragment :: onCallback :: not exists next data");
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("LockerListFragment :: onCallback :: onError() :: status =" + status);
        }
    };
    private ServerProxy.ResponseCallback offCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("LockerListFragment :: offCallback :: onResponse() :: response = " + response);
                DefaultServerParsingModel Modeltemp = new ParseManager().DefaultServerParsing(response);
                if (Modeltemp.getSuccess().equals("1")){
                    Toast.makeText(getActivity(),"사물함 담힘",Toast.LENGTH_SHORT).show();
                }
            } else {
                LogUtil.LOGV("LockerListFragment :: offCallback :: not exists next data");
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("LockerListFragment :: offCallback :: onError() :: status =" + status);
        }
    };

}
