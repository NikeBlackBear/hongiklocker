package com.example.jun.hongiklocker.ViewPager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jun.hongiklocker.Adapter.LogListAdapter;
import com.example.jun.hongiklocker.Model.LogListModel;
import com.example.jun.hongiklocker.Model.ParsingLogListModel;
import com.example.jun.hongiklocker.R;
import com.example.jun.hongiklocker.ServerProxy.ParseManager;
import com.example.jun.hongiklocker.ServerProxy.ServerProxy;
import com.example.jun.hongiklocker.Utils.CommonValues;
import com.example.jun.hongiklocker.Utils.LogUtil;

import java.util.ArrayList;

/**
 * Created by jun on 2015-07-19.
 */
public class LogListFragment extends Fragment implements CommonValues {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private ArrayList<LogListModel> mDataList;
    private ListView mList;
    private LogListModel DummyModel;
    private LogListAdapter mLogListAdapter;
    private String user_id;
    private ImageView no_log;

    public static LogListFragment newInstance(String param1, String param2) {
    LogListFragment fragment = new LogListFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
}

    public LogListFragment() {
        // Required empty public constructor
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        ServerProxy.getInstance(getActivity()).getLogData(getLogInfoCallback, user_id);
    }

    private void init(){
        SharedPreferences pref = getActivity().getSharedPreferences(PREF, getActivity().MODE_PRIVATE);
        user_id = pref.getString(USER_ID,"");
        View view = getView();
        mList = (ListView)view.findViewById(R.id.log_list);
        no_log = (ImageView)view.findViewById(R.id.no_log);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.log_list_fragment, container, false);
    }

    private ServerProxy.ResponseCallback getLogInfoCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("LogListFragment :: getLogInfoCallback :: onResponse() :: response = " + response);
                ParsingLogListModel Modeltemp = new ParseManager().LogListparsing(response);
                if (Modeltemp.getSuccess().equals("1")) {
                    mLogListAdapter = new LogListAdapter(getActivity(), Modeltemp.getMessageinfo());
                    LogUtil.LOGV("LogListFragment :: getLogInfoCallback :: Modeltemp.getMessageinfo() = " + Modeltemp.getMessageinfo().get(0).getLog_user_name());
                    mList.setAdapter(mLogListAdapter);
                    mList.setVisibility(View.VISIBLE);
                    no_log.setVisibility(View.GONE);
                } else {
                    mList.setVisibility(View.GONE);
                    no_log.setVisibility(View.VISIBLE);
                }
            } else {
                LogUtil.LOGV("LogListFragment :: getLogInfoCallback :: not exists next data");
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("LogListFragment :: getLogInfoCallback :: onError() :: status =" + status);
        }
    };
}
