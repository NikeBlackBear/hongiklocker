package com.example.jun.hongiklocker.ServerProxy;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.jun.hongiklocker.Utils.CommonValues;
import com.example.jun.hongiklocker.Utils.LogUtil;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jun on 2015-09-08.
 */
public class ServerProxy implements CommonValues, RequestURL {
    private static ServerProxy mInstance;
    private RequestQueue mRequestQueue;
    private Context mContext;

    private ServerProxy(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static ServerProxy getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ServerProxy(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public interface ResponseCallback {
        void onResponse(String response);

        void onError(int status);
    }


    // login
    public void requestLogin(final ResponseCallback callback, final String ID, final String PW) {
        LogUtil.LOGE("ServerProxy :: requestLogin() :: ID = " + ID + "// PW = " + PW);
        StringRequest request = new StringRequest(Request.Method.POST, LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(LOGIN_ID, ID);
                params.put(LOGIN_PW, PW);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //get Locker information
    public void getLockerInfo(final ResponseCallback callback, final String ID) {
        LogUtil.LOGE("ServerProxy :: requestLogin() :: ID = " + ID);
        StringRequest request = new StringRequest(Request.Method.POST, GET_LOCKER_INFO_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(LOCKER_INFO_USER_ID, ID);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //submit On/Off Signal
    public void submitOnOffSignal(final ResponseCallback callback, final String ID, final String code, final String num, final String tag ) {
        LogUtil.LOGE("ServerProxy :: submitOnOffSignal() :: ID = " + ID +" // code = " + code + " //  num = " + num +" // tag = " + tag);
        StringRequest request = new StringRequest(Request.Method.POST, SUBMIT_LOCKER_ONOFF_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(LOCKER_ONOFF_USER_ID, ID);
                params.put(LOCKER_ONOFF_CODE, code);
                params.put(LOCKER_ONOFF_NUM, num);
                params.put(LOCKER_ONOFF_TAG, tag);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //get Log Data
    public void getLogData(final ResponseCallback callback, final String ID) {
        LogUtil.LOGE("ServerProxy :: submitOnOffSignal() :: ID = " + ID );
        StringRequest request = new StringRequest(Request.Method.POST, GET_LOG_INFO_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(LOG_USER_ID, ID);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

}