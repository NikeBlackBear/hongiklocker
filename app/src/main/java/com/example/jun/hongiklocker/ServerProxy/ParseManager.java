package com.example.jun.hongiklocker.ServerProxy;

import com.example.jun.hongiklocker.Model.DefaultServerParsingModel;
import com.example.jun.hongiklocker.Model.ParsingLockerInfoModel;
import com.example.jun.hongiklocker.Model.ParsingLogListModel;
import com.example.jun.hongiklocker.Utils.CommonValues;
import com.example.jun.hongiklocker.Utils.LogUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Created by jun on 2015-09-09.
 */
public class ParseManager implements CommonValues {
    public ParseManager() {
    }

    // default only has success, message
    public DefaultServerParsingModel DefaultServerParsing(String response){
        LogUtil.LOGV("ParseManager :: DefaultServerParsing :: response = " + response);
        DefaultServerParsingModel temp;
        Gson gson = new Gson();
        temp = gson.fromJson(response, new TypeToken<DefaultServerParsingModel>(){
        }.getType());
        LogUtil.LOGV("ParseManager :: DefaultServerParsing :: temp = " + temp.toString());
        return temp;
    }

    // LockerInfo Parsing
    public ParsingLockerInfoModel LockerInfoparsing(String response){
        LogUtil.LOGV("ParseManager :: LockerInfoparsing :: response = " + response);
        ParsingLockerInfoModel temp;
        Gson gson = new Gson();
        temp = gson.fromJson(response, new TypeToken<ParsingLockerInfoModel>(){
        }.getType());
        LogUtil.LOGV("ParseManager :: LockerInfoparsing :: temp = " + temp.toString());
        return temp;
    }

    // Log Info Parsing
    public ParsingLogListModel LogListparsing(String response){
        LogUtil.LOGV("ParseManager :: LogListparsing :: response = " + response);
        ParsingLogListModel temp;
        Gson gson = new Gson();
        temp = gson.fromJson(response, new TypeToken<ParsingLogListModel>(){
        }.getType());
        LogUtil.LOGV("ParseManager :: LogListparsing :: temp = " + temp.toString());
        return temp;
    }
}
