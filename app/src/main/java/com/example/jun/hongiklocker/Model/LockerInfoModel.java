package com.example.jun.hongiklocker.Model;

/**
 * Created by Administrator on 2015-09-11.
 */
public class LockerInfoModel {
    private String lu_pid;
    private String lu_code;
    private String lu_num;
    private String lu_adu_code;
    private String lu_code_name;

    public void setLu_adu_code(String lu_adu_code) {
        this.lu_adu_code = lu_adu_code;
    }

    public String getLu_adu_code() {
        return lu_adu_code;
    }

    public void setLu_code(String lu_code) {
        this.lu_code = lu_code;
    }

    public void setLu_code_name(String lu_code_name) {
        this.lu_code_name = lu_code_name;
    }

    public String getLu_code_name() {
        return lu_code_name;
    }

    public String getLu_code() {
        return lu_code;
    }

    public void setLu_num(String lu_num) {
        this.lu_num = lu_num;
    }

    public String getLu_num() {
        return lu_num;
    }

    public void setLu_pid(String lu_pid) {
        this.lu_pid = lu_pid;
    }

    public String getLu_pid() {
        return lu_pid;
    }


    @Override
    public String toString(){
        return "lu_pid = "+lu_pid  +"lu_adu_code = "+lu_adu_code +"lu_code = "+lu_code +"lu_num = "+lu_num+"lu_code_name = "+lu_code_name;
    }
}
