package com.example.jun.hongiklocker.Model;

/**
 * Created by jun on 2015-07-20.
 */
public class LogListModel {
    private String user_id;
    private String log_code;
    private String log_num;
    private String log_state;
    private String created_at;
    private String log_code_name;
    private String log_user_name;

    public void setLog_code_name(String log_code_name) {
        this.log_code_name = log_code_name;
    }

    public String getLog_code_name() {
        return log_code_name;
    }

    public void setLog_user_name(String log_user_name) {
        this.log_user_name = log_user_name;
    }

    public String getLog_user_name() {
        return log_user_name;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setLog_code(String log_code) {
        this.log_code = log_code;
    }

    public String getLog_code() {
        return log_code;
    }

    public void setLog_num(String log_num) {
        this.log_num = log_num;
    }

    public String getLog_num() {
        return log_num;
    }

    public void setLog_state(String log_state) {
        this.log_state = log_state;
    }

    public String getLog_state() {
        return log_state;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }
}
