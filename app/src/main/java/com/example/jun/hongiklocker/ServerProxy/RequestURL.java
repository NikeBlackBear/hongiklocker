package com.example.jun.hongiklocker.ServerProxy;

/**
 * Created by jun on 2015-09-08.
 */
public interface RequestURL {
    public static final String LOGIN_URL = "http://203.249.87.220/b089065/page/login.php"; // Login URL
    public static final String GET_LOCKER_INFO_URL = "http://203.249.87.220/b089065/page/get_lock_info.php"; // 사물함 정보를 가져온다
    public static final String SUBMIT_LOCKER_ONOFF_URL = "http://203.249.87.220/b089065/page/action.php"; // 사물함 정보를 가져온다
    public static final String GET_LOG_INFO_URL = "http://203.249.87.220/b089065/page/get_log.php"; // 로그 정보를 가져온다
}
