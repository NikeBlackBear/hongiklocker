package com.example.jun.hongiklocker.main;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jun.hongiklocker.Model.DefaultServerParsingModel;
import com.example.jun.hongiklocker.R;
import com.example.jun.hongiklocker.ServerProxy.ParseManager;
import com.example.jun.hongiklocker.ServerProxy.ServerProxy;
import com.example.jun.hongiklocker.Utils.LogUtil;

public class Login extends FragmentActivity {
    TextView loginBtn;
    EditText id, pw;
    String mId, mPw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mId = "admin";
                mPw = "admin";


//                mId = id.getText().toString();
//                mPw = pw.getText().toString();
                LogUtil.LOGE("Login :: id = " + mId + "// mPw = "+ mPw);
                ServerProxy.getInstance(Login.this).requestLogin(LoginCallback, mId, mPw);
            }
        });
    }
    private void init(){
        loginBtn = (TextView)findViewById(R.id.login_btn);
        id = (EditText)findViewById(R.id.inputid);
        pw = (EditText)findViewById(R.id.input_pw);
    }


    private void goNext(){
        Intent intent = new Intent(Login.this, MainActivity.class);
        intent.putExtra("id",mId);
        startActivity(intent);
        finish();
    }
    private ServerProxy.ResponseCallback LoginCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (Login.this != null) {
                LogUtil.LOGV("Login :: LoginCallback :: onResponse() :: response = " + response);
                DefaultServerParsingModel Modeltemp = new ParseManager().DefaultServerParsing(response);
                if (Modeltemp.getSuccess().equals("1")){
                    goNext();
                }else {
                    Toast.makeText(Login.this,""+getString(R.string.login_fail_message) , Toast.LENGTH_SHORT).show();
                }
            } else {
                LogUtil.LOGV("Login :: LoginCallback :: not exists next data");
            }
        }
        @Override
        public void onError(int status) {
            LogUtil.LOGV("Login :: onError() :: status =" + status);
        }
    };

}
