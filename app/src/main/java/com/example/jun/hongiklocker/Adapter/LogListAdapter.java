package com.example.jun.hongiklocker.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jun.hongiklocker.Model.LogListModel;
import com.example.jun.hongiklocker.R;

import java.util.ArrayList;

/**
 * Created by jun on 2015-07-20.
 */
public class LogListAdapter extends BaseAdapter{
    private Activity mActivity;
    private ArrayList<LogListModel> mlist;
    private LayoutInflater mInflater;

    public LogListAdapter(Activity c, ArrayList<LogListModel> list){
        mActivity =c;
        mInflater = LayoutInflater.from(c);
        mlist = list;
    }
    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public LogListModel getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.log_list_row, null);
            holder = new ViewHolder();
            holder.gua = (TextView)convertView.findViewById(R.id.gua);
            holder.num = (TextView)convertView.findViewById(R.id.num);
            holder.time = (TextView)convertView.findViewById(R.id.time);
            holder.who = (TextView)convertView.findViewById(R.id.who);
            holder.isOpen = (TextView)convertView.findViewById(R.id.isOpen);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        convertView.setTag(holder);
        holder.gua.setText(getItem(position).getLog_code_name());
        holder.num.setText(getItem(position).getLog_num());
        holder.time.setText(getItem(position).getCreated_at());
        holder.who.setText(getItem(position).getLog_user_name());
        if (getItem(position).getLog_state().equals("on")){
            holder.isOpen.setText(R.string.open);
        }else {
            holder.isOpen.setText(R.string.close);
        }

        return convertView;
    }
    private class ViewHolder{
        TextView gua;
        TextView num;
        TextView who;
        TextView time;
        TextView isOpen;
    }
}
