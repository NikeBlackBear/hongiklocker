package com.example.jun.hongiklocker.Utils;

/**
 * Created by jun on 2015-09-08.
 */
public interface CommonValues {

    //preference
    public static final String USER_ID="user_id";
    public static final String PREF = "pref";

    //Login
    public static final String LOGIN_ID = "user_id"; // login id
    public static final String LOGIN_PW = "user_pw"; // login pw

    //locker list
    public static final String LOCKER_INFO_USER_ID = "user_id"; // user_id

    public static final String LOCKER_ONOFF_USER_ID = "user_id"; // user_id
    public static final String LOCKER_ONOFF_CODE = "code"; // locker code (major)
    public static final String LOCKER_ONOFF_NUM = "num"; // locker number
    public static final String LOCKER_ONOFF_TAG = "tag"; // on // off

    //log list
    public static final String LOG_USER_ID = "user_id"; // ouser_id

}
