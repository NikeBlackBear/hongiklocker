package com.example.jun.hongiklocker.main;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.jun.hongiklocker.R;


public class Intro extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        Handler hd = new Handler();
        hd.postDelayed(new Runnable() {
            @Override
            public void run() {
                login();
            }
        }, 1000);
    }
    private void login(){
        Intent intent = new Intent(Intro.this, Login.class);
        startActivity(intent);
        finish();
    }
}
